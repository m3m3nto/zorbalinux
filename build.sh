#!/usr/bin/env bash

# Building process start
echo "#########################################################################"
echo "Start building Zorbalinux..."
echo "#########################################################################"

# Symlinking
echo "#########################################################################"
echo "Creating manjaro-tools symlinks"
echo "#########################################################################"
rm -rf ~/.config/manjaro-tools
mkdir ~/.config/manjaro-tools
ln -s $PWD/manjaro-tools/manjaro-tools.conf ~/.config/manjaro-tools/manjaro-tools.conf

echo "#########################################################################"
echo "Creating zorbalinux manjaro-tools symlinks"
echo "#########################################################################"
sudo rm -rf /usr/share/manjaro-tools/iso-profiles/zorbalinux
sudo mkdir /usr/share/manjaro-tools/iso-profiles/zorbalinux
sudo ln -s $PWD/iso-profiles/zorbalinux/zi3 /usr/share/manjaro-tools/iso-profiles/zorbalinux/zi3

# Building packages
echo "#########################################################################"
echo "Building local packages..."
echo "#########################################################################"
sudo rm -rf online-repo/x86_64/*
sudo rm -rf pkgbuild
mkdir pkgbuild

echo "#########################################################################"
echo "Building siji-git..."
echo "#########################################################################"
git clone https://aur.archlinux.org/siji-git pkgbuild/siji-git
cd pkgbuild
sudo buildpkg -p siji-git -cw
#sudo buildpkg -p siji-git
cd ..
cp -r /var/cache/manjaro-tools/pkg/stable/x86_64/* online-repo/x86_64/

echo "#########################################################################"
echo "Building gscreenshot..."
echo "#########################################################################"
git clone https://aur.archlinux.org/gscreenshot.git pkgbuild/gscreenshot
cd pkgbuild
sudo buildpkg -p gscreenshot -cw
#sudo buildpkg -p gscreenshot
cd ..
cp -r /var/cache/manjaro-tools/pkg/stable/x86_64/* online-repo/x86_64/

# Copyng builded packages into online repos
echo "#########################################################################"
echo "Adding local packages..."
echo "#########################################################################"
cd online-repo/x86_64
repo-add online-repo.db.tar.gz *.pkg.tar.*
cd ../../

echo "#########################################################################"
echo "Syncing remote packages..."
echo "#########################################################################"
#rsync -avz -e "ssh" online-repo/ webdev@loren2:/var/www/apps/manjaro/online-repo/

echo "#########################################################################"
echo "Removing cached packages..."
echo "#########################################################################"
sudo rm -rf /var/cache/pacman/pkg/siji-git*
sudo rm -rf /var/cache/pacman/pkg/gscreenshot*

# Build
echo "#########################################################################"
echo "Building the iso..."
echo "#########################################################################"
sudo rm -rf /var/cache/manjaro-tools/iso/zorbalinux/zi3/
buildiso -p zi3

echo "#########################################################################"
echo "Copyning the iso..."
echo "#########################################################################"
sudo rm -rf iso/*
sudo mv /var/cache/manjaro-tools/iso/zorbalinux/zi3/* iso/

echo "#########################################################################"
echo "Syncing the iso..."
echo "#########################################################################"
#rsync -avz --info=progress2 -e "ssh" iso/ webdev@loren2:/var/www/apps/manjaro/iso/
