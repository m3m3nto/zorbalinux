#!/usr/bin/env bash

echo "#########################################################################"
echo "Kernel and system infos"
uname -a
lsb_release -a
echo "#########################################################################"

# Building process start
echo "#########################################################################"
echo "Start building Zorbalinux..."
echo "#########################################################################"

# Symlinking
echo "#########################################################################"
echo "Creating manjaro-tools symlinks"
echo "#########################################################################"
rm -rf ~/.config/manjaro-tools
mkdir ~/.config
mkdir ~/.config/manjaro-tools
ln -s $PWD/manjaro-tools/manjaro-tools.conf ~/.config/manjaro-tools/manjaro-tools.conf

echo "#########################################################################"
echo "Creating zorbalinux manjaro-tools symlinks"
echo "#########################################################################"
rm -rf /usr/share/manjaro-tools/iso-profiles/zorbalinux
mkdir /usr/share/manjaro-tools/iso-profiles/zorbalinux
ln -s $PWD/iso-profiles/zorbalinux/zi3 /usr/share/manjaro-tools/iso-profiles/zorbalinux/zi3

# Building packages
echo "#########################################################################"
echo "Building local packages..."
echo "#########################################################################"
rm -rf online-repo/x86_64/*
rm -rf pkgbuild
mkdir pkgbuild

echo "#########################################################################"
echo "Building siji-git..."
echo "#########################################################################"
git clone https://aur.archlinux.org/siji-git pkgbuild/siji-git
cd pkgbuild
buildpkg -p siji-git -cw
#buildpkg -p siji-git
cd ..
cp -r /var/cache/manjaro-tools/pkg/stable/x86_64/* online-repo/x86_64/

echo "#########################################################################"
echo "Building gscreenshot..."
echo "#########################################################################"
git clone https://aur.archlinux.org/gscreenshot.git pkgbuild/gscreenshot
cd pkgbuild
buildpkg -p gscreenshot -cw
#buildpkg -p gscreenshot
cd ..
cp -r /var/cache/manjaro-tools/pkg/stable/x86_64/* online-repo/x86_64/

# Copyng builded packages into online repos
echo "#########################################################################"
echo "Adding local packages..."
echo "#########################################################################"
cd online-repo/x86_64
repo-add online-repo.db.tar.gz *.pkg.tar.*
cd ../../

echo "#########################################################################"
echo "Removing cached packages..."
echo "#########################################################################"
rm -rf /var/cache/pacman/pkg/siji-git*
rm -rf /var/cache/pacman/pkg/gscreenshot*

# Build
echo "#########################################################################"
echo "Building the iso..."
echo "#########################################################################"
rm -rf /var/cache/manjaro-tools/iso/zorbalinux/zi3/
buildiso -p zi3
